package pl.trainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import pl.trainer.init.DataInitializer;

@SpringBootApplication
public class TrainerApplication implements ApplicationRunner {

	public static void main(String[] args) {
		SpringApplication.run(TrainerApplication.class, args);
	}

	@Autowired private DataInitializer dataInitializer;
	
	@Override
	public void run(ApplicationArguments args) throws Exception {
		dataInitializer.insertData();		
	}
}

