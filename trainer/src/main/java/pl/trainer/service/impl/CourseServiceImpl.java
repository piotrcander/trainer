package pl.trainer.service.impl;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Course;
import pl.trainer.model.Teacher;
import pl.trainer.repo.CourseRepository;
import pl.trainer.service.CourseService;

@Service
public class CourseServiceImpl implements CourseService {
		
	@Autowired private CourseRepository courseRepo;
	
	@Override
	@Transactional
	public Course addCourse(String name, int numberOfHours, BigDecimal prize, Teacher teacher) {
		Course course = new Course();
		course.setName(name);
		course.setNumberOfHours(numberOfHours);
		course.setPrize(prize);
		course.setTeacher(teacher);
		
		Course savedOne = courseRepo.save(course);
		return savedOne;
	}

	/* READ */
	@Override
	@Transactional
	public Course getCourse(Long courseId) {
		Optional<Course> course = courseRepo.findById(courseId);
		if (course.isPresent()) {
			return course.get();
		} else {
			return null;	
		}

	}
	
	@Transactional(readOnly=true)
	public List<Course> getAllCourses() {
		List<Course> courses = courseRepo.findAll();
		return courses;
	}
	
		
	/* UPDATE */
	@Override
	public Course updateCourse(Long courseId, String name, Long prize, Teacher teacher) {
		Course course = courseRepo.getOne(courseId);
		if (course == null) 
			return null;	
		
		course.setName(name);
		Course savedOne = courseRepo.save(course);
		return savedOne;
		 
	}

	/* DELETE */
	public void deleteCourse(Long courseId) {
		courseRepo.deleteById(courseId);
	}



}
