package pl.trainer.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Client;
import pl.trainer.model.Student;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.service.ClientService;

@Service
public class ClientServiceImpl implements ClientService {

	@Autowired private ClientRepository clientRepo;
	@Autowired private StudentRepository studentRepo;
	
	/* CREATE */
	
	@Override
	@Transactional
	public Client addClient(String name, String city, String zipCode, String street, String nip) {
		Client client = new Client();
		client.setName(name);
		client.setCity(city);
		client.setZipCode(zipCode);
		client.setStreet(street);
		client.setNip(nip);
		Client savedOne = clientRepo.save(client);
		
		return savedOne;
	}

	@Override
	public Client getClient(Long clientId) {
		Optional<Client> client = clientRepo.findById(clientId);
		if (client.isPresent()) {
			return client.get();
		} else {
			return null;
		}
	}

	@Override
	public List<Client> getAllClients() {
		List<Client> clientList = clientRepo.findAll();
		return clientList;
	}

	@Override
	public Client updateClient(Long clientId, String name) {
		Client client = getClient(clientId);
		if (client == null)
		return null;
		
		client.setName(name);
		Client savedOne = clientRepo.save(client);
		return savedOne;
	}

	@Override
	public void deleteClient(Long clientId) {
		List<Student> students = studentRepo.findByClientId(clientId);
		System.out.println(students.size());
		if (students.size()==0) {
			clientRepo.deleteById(clientId);
		} else {
			
		}
		
		
	}
	
}
