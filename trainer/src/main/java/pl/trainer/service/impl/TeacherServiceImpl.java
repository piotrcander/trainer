package pl.trainer.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Teacher;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.repo.TeacherRepository;
import pl.trainer.service.StudentService;
import pl.trainer.service.TeacherService;

@Service
public class TeacherServiceImpl implements TeacherService{
	
	@Autowired private TeacherRepository teacherRepo;
	@Autowired private CourseRepository courseRepo;
	
	public DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	private LocalDate parseDate(String date) {
		if (date == null)
			return null;
		else
			return LocalDate.parse(date, dateFormatter);
	}
	
	@Override
	@Transactional
	public Teacher addTeacher(String firstName, String lastName, String hireDate) {
		Teacher teacher = new Teacher();
		teacher.setFirstName(firstName);
		teacher.setLastName(lastName);
		teacher.setHireDate(parseDate(hireDate));
		
		Teacher savedOne = teacherRepo.save(teacher);
		return savedOne;
	}

	@Override
	public Teacher getTeacher(Long teacherId) {
		Optional<Teacher> teacher = teacherRepo.findById(teacherId);
		
		if (teacher.isPresent()) {
			return teacher.get();
		} else {
			return null;
		}
	}

	@Override
	public List<Teacher> getAllTeachers() {
		List<Teacher> teachers = teacherRepo.findAll();
		return teachers;
	}

	@Override
	@Transactional
	public Teacher updateTeacher(Long teacherId, String firstName, String lastName, String hireDate) {
		Teacher teacher = getTeacher(teacherId);
		if (teacher == null)
			return null;
		
		teacher.setFirstName(firstName);
		teacher.setLastName(lastName);
		teacher.setHireDate(parseDate(hireDate));
		
		Teacher savedOne = teacherRepo.save(teacher);
		return savedOne;
	}

	@Override
	public void deleteTeacher(Long teacherId) {
		teacherRepo.deleteById(teacherId);
	}








}
