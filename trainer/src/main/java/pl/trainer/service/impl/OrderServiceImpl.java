package pl.trainer.service.impl;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Client;
import pl.trainer.model.Course;
import pl.trainer.model.Order;
import pl.trainer.model.Student;
import pl.trainer.repo.OrderRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.service.OrderService;

@Service
public class OrderServiceImpl implements OrderService {

	@Autowired private OrderRepository orderRepo;
	@Autowired private StudentRepository studentRepo;
	
	public DateTimeFormatter dateFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

	public LocalDate parseDate(String date) {
		if (date == null)
			return null;
		else
			return LocalDate.parse(date, dateFormatter);
	}
	
	/* CREATE */
	@Transactional
	public Order addOrder (Student student, Course course, String startDate) {
		Order order = new Order();
		order.setStudent(student);
		order.setCourse(course);
		order.setStartDate(parseDate(startDate));
		
		Order savedOne = orderRepo.save(order);
		return savedOne;
	}
	
	/* READ */
	@Transactional(readOnly=true)
	public Order getOrder(Long orderId) {
		Optional<Order> order = orderRepo.findById(orderId);
		
		if (order.isPresent()) {
			return order.get();
		} else {
			return null;
		}
		
	}

	@Override
	public List<Order> getAllOrders() {
		List<Order> orders = orderRepo.findAll();
		return orders;
	}

	@Transactional
	public List<Order> getOrdersByStudent(Order order) {
		List<Order> orders = orderRepo.findByStudentId(order.getStudent().getId());
		return orders;
	}

	@Transactional
	public List<Order> getOrdersByCourse(Order order) {
		List<Order> orders = orderRepo.findByCourseId(order.getCourse().getId());
		return orders;
	}

	@Override
	public Order updateOrder(Long orderId, Student student, Course course, String startDate) {
		Order order = getOrder(orderId);
		if (order==null)
			return null;
		
		order.setCourse(course);
		order.setStartDate(parseDate(startDate));
		
		Order updatedOrder = orderRepo.save(order);
		return updatedOrder;
	}

	@Override
	public void deleteOrder(Long orderId) {
		orderRepo.deleteById(orderId);
		
	}

}
