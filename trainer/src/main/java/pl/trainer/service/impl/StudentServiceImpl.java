package pl.trainer.service.impl;

import java.time.LocalDate;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Student;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.service.StudentService;

@Service
public class StudentServiceImpl implements StudentService{
	
	@Autowired private StudentRepository studentRepo;
	
	/* CREATE */
	@Override
	@Transactional
	public Student addStudent(String firstName, String lastName, String email, int birthYear, String city) {
		Student student = new Student();
		student.setFirstName(firstName);
		student.setLastName(lastName);
		student.setEmail(email);
		student.setBirthYear(birthYear);
		student.setCity(city);
		
		Student savedOne = studentRepo.save(student);
		return savedOne;
	}

	/* READ */
	@Transactional(readOnly=true)
	public Student getStudent(Long studentId) {
		Optional<Student> student = studentRepo.findById(studentId);
		
		if (student.isPresent()) {
			return student.get();
		} else {
			return null;
		}
	}
	
	@Transactional(readOnly=true)
	public List<Student> getAllStudents() {
		List<Student> students= studentRepo.findAll();
		return students;
	}

	@Transactional(readOnly=true)
	public List<Student> getStudentByClient(Student student) {
		List<Student> students = studentRepo.findByClientId(student.getClient().getId());
		return students;
	}	
	
	/* UPDATE */
	public Student updateStudent(Long studentId, String firstName, String lastName, String email, int birthYear, String city) {
		Student student = getStudent(studentId);
		if (student == null)
			return null;
		
		student.setFirstName(firstName);
		student.setLastName(lastName);
		student.setEmail(email);
		student.setBirthYear(birthYear);
		student.setCity(city);
		
		Student savedOne = studentRepo.save(student);
		return savedOne;
	}

	/* DELETE */
	public void deleteStudent(Long studentId) {
		studentRepo.deleteById(studentId);
	}



}
