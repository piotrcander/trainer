package pl.trainer.service;

import java.util.List;

import pl.trainer.model.Client;

public interface ClientService {
	
	Client addClient(String name, String city, String zipCode, String street, String nip);

	Client getClient(Long id);

	List<Client> getAllClients();

	Client updateClient(Long id, String name);

	void deleteClient(Long id);
}
