package pl.trainer.service;

import java.util.List;

import pl.trainer.model.Student;

public interface StudentService {
	
	Student addStudent(String firstName, String lastName, String email, int birthYear, String city);

	Student getStudent(Long studentId);

	List<Student> getAllStudents();

	Student updateStudent(Long studentId, String firstName, String lastName, String email, int birthYear, String city);

	void deleteStudent(Long studentId);


}
