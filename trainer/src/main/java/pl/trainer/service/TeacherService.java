package pl.trainer.service;

import java.util.List;

import pl.trainer.model.Teacher;

public interface TeacherService {
	
	Teacher addTeacher(String firstName, String lastName, String hireDate);

	Teacher getTeacher(Long teacherId);

	List<Teacher> getAllTeachers();

	Teacher updateTeacher(Long teacherId, String firstName, String lastName, String hireDate);

	void deleteTeacher(Long teacherId);


}
