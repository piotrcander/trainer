package pl.trainer.service;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Client;
import pl.trainer.model.Course;
import pl.trainer.model.Teacher;

@Service
public interface CourseService {
	
	Course addCourse(String name, int numberOfHours, BigDecimal prize, Teacher teacher);

	Course getCourse(Long CourseId);

	List<Course> getAllCourses();

	Course updateCourse(Long courseId, String name, Long prize, Teacher teacher);

	void deleteCourse(Long courseId);

	
}
