package pl.trainer.service;

import java.time.LocalDate;
import java.util.List;

import pl.trainer.model.Client;
import pl.trainer.model.Course;
import pl.trainer.model.Order;
import pl.trainer.model.Student;

public interface OrderService {

	Order addOrder(Student student, Course course, String startDate);

	Order getOrder(Long orderId);
		
	List<Order> getAllOrders();

	Order updateOrder(Long orderId, Student student, Course course, String startDate);

	void deleteOrder (Long orderId);

}
