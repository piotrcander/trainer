package pl.trainer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Email;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import org.springframework.beans.factory.annotation.Value;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "students")
@NoArgsConstructor 
@Getter 
@Setter
public class Student {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "student_id")
	private Long id;
	
	@Email
	@Column(nullable = false)
	private String email;
	
	@Size(min=2, max=20)
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Size(min=2, max=30)
	@Column(name = "last_name", nullable = false)
	private String lastName;
	
	@Min(1900)
	@Max(2018)
	@Column(name = "birth_year", nullable = true)
	private int birthYear;
	
	@Size(min=2, max=30)
	@Column(name = "city", length = 20, nullable = true)
	private String city;
	
	@ManyToOne
	@JoinColumn(name = "client_id")	
	private Client client;
	

}
