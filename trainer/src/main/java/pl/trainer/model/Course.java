package pl.trainer.model;

import java.math.BigDecimal;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name= "courses")
@NoArgsConstructor 
@Getter 
@Setter
public class Course {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "course_id")
	private Long id;

	@Column(name = "name")
	@Size(min=2, max=100)
	@NotNull
	private String name;
	
	@Column(name = "number_of_hours")
	@NotNull
	private int numberOfHours;
	
	@Column(name = "prize")
	private BigDecimal prize;
	
	@ManyToOne
	@JoinColumn(name = "teacher_id" )
	private Teacher teacher;

}
