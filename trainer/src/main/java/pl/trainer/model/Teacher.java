package pl.trainer.model;

import java.math.BigDecimal;
import java.time.LocalDate;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import org.springframework.format.annotation.DateTimeFormat;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "teachers")
@NoArgsConstructor 
@Getter 
@Setter
public class Teacher {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "teacher_id")
	private Long id;
	
	@Size(min=2, max=20)
	@Column(name = "first_name", nullable = false)
	private String firstName;
	
	@Size(min=2, max=30)
	@Column(name = "last_name", nullable = false)
	private String lastName;
	
	@Column(name = "hire_date", nullable = true)
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate hireDate;

	
}
