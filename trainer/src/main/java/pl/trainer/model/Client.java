package pl.trainer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "clients")
@NoArgsConstructor 
@Getter 
@Setter
public class Client {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "client_id")
	private Long id;

	@Size(min=2, max=40)
	@Column(name = "name", nullable = false)
	private String name;
	
	@Size(min=2, max=20)
	@Column(name = "city", nullable = false)
	private String city;
	
	@Size(min=5, max=5)
	@Column(name = "zipCode", length = 5, nullable = true)
	private String zipCode;
	
	@Size(min=2, max=40)
	@Column(name = "street", nullable = false)
	private String street;
	
	@Size(min=10, max=10)
	@Column(name = "nip", length = 10, nullable = false)
	private String nip;
}
	
