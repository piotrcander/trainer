package pl.trainer.init;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pl.trainer.model.Client;
import pl.trainer.model.Course;
import pl.trainer.model.Order;
import pl.trainer.model.Student;
import pl.trainer.model.Teacher;
import pl.trainer.service.ClientService;
import pl.trainer.service.CourseService;
import pl.trainer.service.OrderService;
import pl.trainer.service.StudentService;
import pl.trainer.service.TeacherService;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

@Service("dataInitializer")
@Repository
@Transactional
public class DataInitializer {

	@Autowired private StudentService studentSrv;
	@Autowired private ClientService clientSrv;
	@Autowired private CourseService courseSrv;
	@Autowired private TeacherService teacherSrv;
	@Autowired private OrderService orderSrv;
	
	public void insertData() {
		
		Teacher lukasz = teacherSrv.addTeacher("Łukasz", "Nowak", "2018-02-01");
		Teacher marek = teacherSrv.addTeacher("Marek", "Dąbrowski", "2017-06-01");
		Teacher krzysiu = teacherSrv.addTeacher("Krzysztof", "Krawczyk", "2018-05-15");
		
		Course excell = courseSrv.addCourse("Excell dla początkujących", 8, BigDecimal.valueOf(1200.00), marek);
		Course access = courseSrv.addCourse("Zarządzenie bazami danych MS Access", 16, BigDecimal.valueOf(1700.00), krzysiu);
		Course sql = courseSrv.addCourse("Zapytania MySQL", 24, BigDecimal.valueOf(1000.00), lukasz);
		Course php = courseSrv.addCourse("Programowanie PHP dla zaawansowanych", 12, BigDecimal.valueOf(2000.00), lukasz);
		Course js = courseSrv.addCourse("Javascript bez tajemnic", 16, BigDecimal.valueOf(1300.00), marek);
		
		Client santander = clientSrv.addClient("Santander", "Warszawa", "00121", "Robotnicza 5", "8771602323");
		Client amazon = clientSrv.addClient("Amazon", "Sady", "61045", "Łąkowa 1", "8699121663");
		Client lidl = clientSrv.addClient("Lidl", "Tarnowo Podgórne", "62020", "Poznańska 123", "8882021191");
		Client philips = clientSrv.addClient("Philips", "Piła", "67400", "Krańcowa 17-19", "8661502663");
		
		Student hanka = studentSrv.addStudent("Hanka","Mostowiak","hanka@gmail.com", 2001,"Lipnica");
		hanka.setClient(santander);
		Student piotr = studentSrv.addStudent("Piotr","Żyła","piecia@wp.pl", 2000,"Wisła");
		piotr.setClient(santander);
		Student kinga = studentSrv.addStudent("Kinga","Zduńska","sloneczko123@onet.pl", 1989,"Warszawa");
		kinga.setClient(lidl);
		Student marcin = studentSrv.addStudent("Marcin","Mroczek","marcin.m@wp.pl", 1996,"Górki");
		marcin.setClient(philips);
		Student wojtek = studentSrv.addStudent("Wojciech","Szczęsny","wojtas2001@gmail.com", 1999,"Piaseczno");
		wojtek.setClient(lidl);
		Student henryk = studentSrv.addStudent("Henryk","Sienkiewicz","henn@gmail.com", 1979,"Lublin");
		henryk.setClient(amazon);
		Student ewa = studentSrv.addStudent("Ewa", "Chodakowska", "ewa@chodakowska.pl", 1990, "Wrocław");
		ewa.setClient(amazon);
		
		Order o1 = orderSrv.addOrder(marcin, sql, "2019-04-20");
		Order o2 = orderSrv.addOrder(wojtek, excell, "2019-04-15");
		Order o3 = orderSrv.addOrder(kinga, excell, "2019-04-15");
		Order o4 = orderSrv.addOrder(marcin, access, "2019-03-28");
		
	}
	
	
}
