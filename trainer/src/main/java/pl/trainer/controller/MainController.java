package pl.trainer.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.StudentRepository;

@Controller
public class MainController {
	
	@Autowired private StudentRepository studentRepo;
	@Autowired private ClientRepository clientRepo;
	@Autowired private CourseRepository courseRepo;
	
	@RequestMapping(value= {"","/"})
	public String getMainPage(Model model) {
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		return "main";
	}
	

}
