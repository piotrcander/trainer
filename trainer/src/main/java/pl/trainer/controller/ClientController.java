package pl.trainer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.trainer.model.Client;
import pl.trainer.model.Student;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.service.ClientService;

@Controller
@RequestMapping("/client")
public class ClientController {
	@Autowired private StudentRepository studentRepo;
	@Autowired private ClientRepository clientRepo;
	@Autowired private CourseRepository courseRepo;
	
	@RequestMapping("/get/{id}")
	public String getClientById(@PathVariable("id") Long id, Model model) {
		Client client= clientRepo.getOne(id);
		model.addAttribute(client);
		return "getClient";
	}
	
	@RequestMapping("/getAll")
	public String getAllClients(Model model) {
		List<Client> clientList = clientRepo.findAll();
		model.addAttribute(clientList);
		return "getAllClients";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showNewClientForm(Model model) {
		model.addAttribute(new Client());
		return "newClientForm";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addClient(@Valid Client client, Errors errors, Model model) {
		if (errors.hasErrors()) {
			return "editClientForm2";
		}
		Client newClient = clientRepo.save(client);
		return "redirect:/client/get/" + newClient.getId();
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEditClientForm(@PathVariable("id") Long id, Model model) {
		Client client= clientRepo.getOne(id);
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(client);
		return "editClientForm";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String updateClient(@Valid Client client, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			return "editClientForm2";
		}
		Client updatedClient = clientRepo.save(client);
		return "redirect:/client/get/" + updatedClient.getId();
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String deleteClient(@PathVariable("id") Long id, Model model) {
		Client client = clientRepo.getOne(id);
		model.addAttribute(client);
		List<Student> students = studentRepo.findByClientId(client.getId());
		if (students.size() == 0 ) {
			clientRepo.deleteById(id);
			return "deleteClient";
		} else {
			return "deleteClient2";
		}
	}
}
