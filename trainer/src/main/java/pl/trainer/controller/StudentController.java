package pl.trainer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.trainer.model.Order;
import pl.trainer.model.Student;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.OrderRepository;
import pl.trainer.repo.StudentRepository;

@Controller
@RequestMapping("/student")
public class StudentController {

	@Autowired private StudentRepository studentRepo;
	@Autowired private ClientRepository clientRepo;
	@Autowired private CourseRepository courseRepo;
	@Autowired private OrderRepository orderRepo;
	
	@RequestMapping("/get/{id}")
	public String getStudentById(@PathVariable("id") Long id, Model model) {
		Student student = studentRepo.getOne(id);
		model.addAttribute(student);
		return "getStudent";
	}
	
	@RequestMapping("/getAll")
	public String getAllStudents(Model model) {
		List<Student> studentList = studentRepo.findAll();
		model.addAttribute(studentList);
		return "getAllStudents";
	}

	@RequestMapping("/getByClient/{clientId}")
	public String getStudentsByClient(@PathVariable("clientId") Long clientId, Model model) {
		List<Student> studentList = studentRepo.findByClientId(clientId);
		model.addAttribute(studentList);
		return "getAllStudents2";
	}	
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showNewStudentForm(@Valid Model model) {
		model.addAttribute(new Student());
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		return "newStudentForm";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addStudent(@Valid Student student, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			return "editStudentForm2";
		}
		Student newStudent = studentRepo.save(student);
		return "redirect:/student/get/" + newStudent.getId();
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEditStudentForm(@PathVariable("id") Long id, Model model) {
		Student student = studentRepo.getOne(id);
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(student);
		return "editStudentForm";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String updateStudent(@Valid Student student, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			return "editStudentForm2";
		}
		Student updatedStudent = studentRepo.save(student);
		return "redirect:/student/get/" + updatedStudent.getId();
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String deleteStudent(@PathVariable("id") Long id, Model model) {
		Student student = studentRepo.getOne(id);
		model.addAttribute(student);
		List<Order> orders = orderRepo.findByStudentId(student.getId());
		for (Order order : orders) {
			orderRepo.delete(order);
		}
		studentRepo.deleteById(id);
		return "deleteStudent";
	}
}
