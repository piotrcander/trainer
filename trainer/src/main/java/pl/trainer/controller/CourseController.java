package pl.trainer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.trainer.model.Client;
import pl.trainer.model.Course;
import pl.trainer.model.Order;
import pl.trainer.model.Student;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.OrderRepository;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.StudentRepository;
import pl.trainer.repo.TeacherRepository;

@Controller
@RequestMapping("/course")
public class CourseController {
	@Autowired private StudentRepository studentRepo;
	@Autowired private CourseRepository courseRepo;
	@Autowired private ClientRepository clientRepo;
	@Autowired private TeacherRepository teacherRepo;
	@Autowired private OrderRepository orderRepo;
	
	
	@RequestMapping("/get/{id}")
	public String getCourseById(@PathVariable("id") Long id, Model model) {
		Course course= courseRepo.getOne(id);
		model.addAttribute(course);
		return "getCourse";
	}
	
	@RequestMapping("/getByTeacher/{teacherId}")
	public String getCoursesByTeacherId(@PathVariable("teacherId") Long teacherId, Model model) {
		List<Course> courseList = courseRepo.findByTeacherId(teacherId);
		model.addAttribute(courseList);
		return "getAllCourses2";
	}
	
	@RequestMapping("/getAll")
	public String getAllCourses(Model model) {
		List<Course> courseList = courseRepo.findAll();
		model.addAttribute(courseList);
		return "getAllCourses";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showNewCourseForm(Model model) {
		model.addAttribute(new Course());
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute("teacherList", teacherRepo.findAll());
		return "newCourseForm";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addCourse(@Valid Course course, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			model.addAttribute("teacherList", teacherRepo.findAll());
			return "editCourseForm2";
		}
		Course newCourse = courseRepo.save(course);
		return "redirect:/course/get/" + newCourse.getId();
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEditCourseForm(@PathVariable("id") Long id, Model model) {
		Course course= courseRepo.getOne(id);
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute("teacherList", teacherRepo.findAll());
		model.addAttribute(course);
		return "editCourseForm";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String updateCourse(@Valid Course course, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			model.addAttribute("teacherList", teacherRepo.findAll());
			return "editCourseForm2";
		}
		Course updatedCourse = courseRepo.save(course);
		return "redirect:/course/get/" + updatedCourse.getId();
	}

	@RequestMapping(value = "/delete/{id}")
	public String deleteCourse(@PathVariable("id") Long id, Model model) {
		Course course = courseRepo.getOne(id);
		model.addAttribute(course);
		List<Order> orders = orderRepo.findByCourseId(course.getId());
		for (Order order : orders) {
			orderRepo.delete(order);
		}
		courseRepo.deleteById(id);
		return "deleteCourse";
	}
	
}
