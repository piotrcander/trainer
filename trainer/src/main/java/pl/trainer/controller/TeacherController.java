package pl.trainer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.trainer.model.Course;
import pl.trainer.model.Teacher;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.TeacherRepository;

@Controller
@RequestMapping("/teacher")
public class TeacherController {

	@Autowired private CourseRepository courseRepo;
	@Autowired private TeacherRepository teacherRepo;
	
	@RequestMapping("/get/{id}")
	public String getTeacherById(@PathVariable("id") Long id, Model model) {
		Teacher teacher= teacherRepo.getOne(id);
		model.addAttribute(teacher);
		return "getTeacher";
	}
	
	@RequestMapping("/getAll")
	public String getAllTeachers(Model model) {
		List<Teacher> teacherList = teacherRepo.findAll();
		model.addAttribute(teacherList);
		return "getAllTeachers";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showNewTeacherForm(Model model) {
		model.addAttribute(new Teacher());
		model.addAttribute("courseList", courseRepo.findAll());
		return "newTeacherForm";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addTeacher(@Valid Teacher teacher, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("courseList", courseRepo.findAll());
			return "editTeacherForm";
		}
		
		Teacher newTeacher= teacherRepo.save(teacher);
		return "redirect:/teacher/get/" + newTeacher.getId();
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEditTeacherForm(@PathVariable("id") Long id, Model model) {
		Teacher teacher = teacherRepo.getOne(id);
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(teacher);
		return "editTeacherForm";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String updateTeacher(@Valid Teacher teacher, Errors errors, Model model) {
		if (errors.hasErrors()) {

			model.addAttribute("teacherList", teacherRepo.findAll());
			return "editTeacherForm";
		}
		Teacher updatedTeacher = teacherRepo.save(teacher);
		return "redirect:/teacher/get/" + updatedTeacher.getId();
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String deleteTeacher(@PathVariable("id") Long id, Model model) {
		Teacher teacher = teacherRepo.getOne(id);
		model.addAttribute(teacher);
		List<Course> courses = courseRepo.findByTeacherId(teacher.getId());
		
		for (Course course : courses) {
			course.setTeacher(null);
		}
		
		teacherRepo.deleteById(id);
		return "deleteTeacher";
	}
}
