package pl.trainer.controller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.Errors;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import pl.trainer.model.Student;
import pl.trainer.repo.ClientRepository;
import pl.trainer.repo.CourseRepository;
import pl.trainer.repo.OrderRepository;
import pl.trainer.repo.StudentRepository;

import pl.trainer.model.Order;

@Controller
@RequestMapping("/order")
public class OrderController {
	@Autowired private StudentRepository studentRepo;
	@Autowired private ClientRepository clientRepo;
	@Autowired private CourseRepository courseRepo;
	@Autowired private OrderRepository orderRepo;
	
	@RequestMapping("/get/{id}")
	public String getOrderById(@PathVariable("id") Long id, Model model) {
		Order order = orderRepo.getOne(id);
		model.addAttribute(order);
		return "getOrder";
	}
	
	@RequestMapping("/getAll")
	public String getAllOrders(Model model) {
		List<Order> orderList = orderRepo.findAll();
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(orderList);
		return "getAllOrders";
	}
	
	@RequestMapping("/getByStudent/{studentId}")
	public String getOrdersByStudentId(@PathVariable("studentId") Long studentId, Model model) {
		List<Order> orderList = orderRepo.findByStudentId(studentId);
		model.addAttribute(orderList);
		return "getAllOrders";
	}
		
	@RequestMapping(value = "/getByCourse/{courseId}", method = RequestMethod.GET)
	public String getOrdersByCourseId(@PathVariable("courseId") Long courseId, Model model) {
		List<Order> orderList = orderRepo.findByCourseId(courseId);
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(orderList);
		return "getAllOrders2";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.GET)
	public String showNewOrderForm(@Valid Model model) {
		model.addAttribute(new Order());
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		return "newOrderForm";
	}
	
	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String addOrder(@Valid Order order, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			return "newOrderForm2";
		}
		Order newOrder = orderRepo.save(order);
		return "redirect:/order/get/" + newOrder.getId();
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.GET)
	public String showEditOrderForm(@PathVariable("id") Long id, Model model) {
		Order order = orderRepo.getOne(id);
		model.addAttribute("studentList", studentRepo.findAll());
		model.addAttribute("clientList", clientRepo.findAll());
		model.addAttribute("courseList", courseRepo.findAll());
		model.addAttribute(order);
		return "editOrderForm";
	}
	
	@RequestMapping(value = "/edit/{id}", method = RequestMethod.POST)
	public String updateOrder(@Valid Order order, Errors errors, Model model) {
		if (errors.hasErrors()) {
			model.addAttribute("studentList", studentRepo.findAll());
			model.addAttribute("clientList", clientRepo.findAll());
			model.addAttribute("courseList", courseRepo.findAll());
			return "editOrderForm";
		}
		Order updatedOrder = orderRepo.save(order);
		return "redirect:/order/get/" + updatedOrder.getId();
	}
	
	@RequestMapping(value = "/delete/{id}")
	public String deleteOrder(@PathVariable("id") Long id, Model model) {
		Order order = orderRepo.getOne(id);
		model.addAttribute(order);
		orderRepo.deleteById(id);
		return "deleteOrder";
	}
}
