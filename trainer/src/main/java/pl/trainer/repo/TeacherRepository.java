package pl.trainer.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.trainer.model.Teacher;

public interface TeacherRepository extends JpaRepository<Teacher, Long> {

}
