package pl.trainer.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.trainer.model.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
	
	public List<Student> findByClientId(Long clientId);

}
