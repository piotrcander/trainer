package pl.trainer.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.trainer.model.Client;

public interface ClientRepository extends JpaRepository<Client, Long> {
		
	 public Optional<Client> findByName(String name); 
}
