package pl.trainer.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.trainer.model.Course;

public interface CourseRepository extends JpaRepository<Course, Long> {

	public Optional<Course> findByName(String name); 
	
	public List<Course> findByTeacherId(Long teacherId);
	
}
