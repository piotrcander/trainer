package pl.trainer.repo;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import pl.trainer.model.Order;

public interface OrderRepository extends JpaRepository<Order, Long>  {

	List<Order> findByStudentId(Long studentId);
	
	List<Order> findByCourseId(Long courseId);
}
